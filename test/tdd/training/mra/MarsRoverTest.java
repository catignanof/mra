package tdd.training.mra;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

public class MarsRoverTest {

	@Test
	public void testObstaclePosition() throws Exception {
		ArrayList<String> planetObstacles = new ArrayList<String>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover mr = new MarsRover(0, 0, planetObstacles);
		
		assertTrue(mr.planetContainsObstacleAt(4, 7));
	}
	
	
	@Test
	public void emptyCommandStringShouldReturnStartingPosition() throws Exception {
		ArrayList<String> planetObstacles = new ArrayList<String>();
		MarsRover mr = new MarsRover(0, 0, planetObstacles);
		
		assertEquals("(0,0,N)", mr.executeCommand(""));
	}
	
	
	@Test
	public void testTurningLeftFromNorth() throws Exception {
		ArrayList<String> planetObstacles = new ArrayList<String>();
		MarsRover mr = new MarsRover(0, 0, planetObstacles);
		
		assertEquals("(0,0,W)", mr.executeCommand("l"));
	}
	
	
	@Test
	public void testTurningLeftFromWest() throws Exception {
		ArrayList<String> planetObstacles = new ArrayList<String>();
		MarsRover mr = new MarsRover(0, 0, planetObstacles);
		
		mr.executeCommand("l");
		
		assertEquals("(0,0,S)", mr.executeCommand("l"));
	}
	
	
	@Test
	public void testTurningLeftFromSouth() throws Exception {
		ArrayList<String> planetObstacles = new ArrayList<String>();
		MarsRover mr = new MarsRover(0, 0, planetObstacles);
		
		mr.executeCommand("l");
		mr.executeCommand("l");
		
		assertEquals("(0,0,E)", mr.executeCommand("l"));
	}
	
	
	@Test
	public void testTurningLeftFromEast() throws Exception {
		ArrayList<String> planetObstacles = new ArrayList<String>();
		MarsRover mr = new MarsRover(0, 0, planetObstacles);
		
		mr.executeCommand("l");
		mr.executeCommand("l");
		mr.executeCommand("l");
		
		assertEquals("(0,0,N)", mr.executeCommand("l"));
	}
	
	
	@Test
	public void testTurningRightFromNorth() throws Exception {
		ArrayList<String> planetObstacles = new ArrayList<String>();
		MarsRover mr = new MarsRover(0, 0, planetObstacles);
		
		assertEquals("(0,0,E)", mr.executeCommand("r"));
	}
	
	
	@Test
	public void testTurningRightFromEast() throws Exception {
		ArrayList<String> planetObstacles = new ArrayList<String>();
		MarsRover mr = new MarsRover(0, 0, planetObstacles);
		
		mr.executeCommand("r");
		
		assertEquals("(0,0,S)", mr.executeCommand("r"));
	}
	
	
	@Test
	public void testTurningRightFromSouth() throws Exception {
		ArrayList<String> planetObstacles = new ArrayList<String>();
		MarsRover mr = new MarsRover(0, 0, planetObstacles);
		
		mr.executeCommand("r");
		mr.executeCommand("r");
		
		assertEquals("(0,0,W)", mr.executeCommand("r"));
	}
	
	
	@Test
	public void testTurningRightFromWest() throws Exception {
		ArrayList<String> planetObstacles = new ArrayList<String>();
		MarsRover mr = new MarsRover(0, 0, planetObstacles);
		
		mr.executeCommand("l");
		
		assertEquals("(0,0,N)", mr.executeCommand("r"));
	}
	
	
	@Test
	public void testMovingForwardFromNorth() throws Exception {
		ArrayList<String> planetObstacles = new ArrayList<String>();
		MarsRover mr = new MarsRover(0, 0, planetObstacles);
		
		assertEquals("(0,1,N)", mr.executeCommand("f"));
	}
	
	
	@Test
	public void testMovingForwardFromEast() throws Exception {
		ArrayList<String> planetObstacles = new ArrayList<String>();
		MarsRover mr = new MarsRover(2, 2, planetObstacles);
		
		mr.executeCommand("r");
		
		assertEquals("(3,2,E)", mr.executeCommand("f"));
	}
	
	
	@Test
	public void testMovingForwardFromSouth() throws Exception {
		ArrayList<String> planetObstacles = new ArrayList<String>();
		MarsRover mr = new MarsRover(2, 2, planetObstacles);
		
		mr.executeCommand("r");
		mr.executeCommand("r");
		
		assertEquals("(2,1,S)", mr.executeCommand("f"));
	}
	
	
	@Test
	public void testMovingForwardFromWest() throws Exception {
		ArrayList<String> planetObstacles = new ArrayList<String>();
		MarsRover mr = new MarsRover(2, 2, planetObstacles);
		
		mr.executeCommand("l");
		
		assertEquals("(1,2,W)", mr.executeCommand("f"));
	}
	
	
	@Test
	public void testMovingBackwardFromNorth() throws Exception {
		ArrayList<String> planetObstacles = new ArrayList<String>();
		MarsRover mr = new MarsRover(2, 2, planetObstacles);
		
		assertEquals("(2,1,N)", mr.executeCommand("b"));
	}
	
	
	@Test
	public void testMovingBackwardFromEast() throws Exception {
		ArrayList<String> planetObstacles = new ArrayList<String>();
		MarsRover mr = new MarsRover(2, 2, planetObstacles);
		
		mr.executeCommand("r");
		
		assertEquals("(1,2,E)", mr.executeCommand("b"));
	}
	
	
	@Test
	public void testMovingBackwardFromSouth() throws Exception {
		ArrayList<String> planetObstacles = new ArrayList<String>();
		MarsRover mr = new MarsRover(2, 2, planetObstacles);
		
		mr.executeCommand("r");
		mr.executeCommand("r");
		
		assertEquals("(2,3,S)", mr.executeCommand("b"));
	}
	
	
	@Test
	public void testMovingBackwardFromWest() throws Exception {
		ArrayList<String> planetObstacles = new ArrayList<String>();
		MarsRover mr = new MarsRover(2, 2, planetObstacles);
		
		mr.executeCommand("l");
		
		assertEquals("(3,2,W)", mr.executeCommand("b"));
	}
	
	
	@Test
	public void testMovingCombined() throws Exception {
		ArrayList<String> planetObstacles = new ArrayList<String>();
		MarsRover mr = new MarsRover(0, 0, planetObstacles);
		
		assertEquals("(2,2,E)", mr.executeCommand("ffrff"));
	}
	
	
	@Test
	public void testWrappingWithBackwardMovementFromNorth() throws Exception {
		ArrayList<String> planetObstacles = new ArrayList<String>();
		MarsRover mr = new MarsRover(0, 0, planetObstacles);
		
		assertEquals("(0,9,N)", mr.executeCommand("b"));
	}
	
	
	@Test
	public void testWrappingWithBackwardMovementFromEast() throws Exception {
		ArrayList<String> planetObstacles = new ArrayList<String>();
		MarsRover mr = new MarsRover(0, 0, planetObstacles);
		
		assertEquals("(9,0,E)", mr.executeCommand("rb"));
	}
	
	
	@Test
	public void testWrappingWithForwardMovementFromWest() throws Exception {
		ArrayList<String> planetObstacles = new ArrayList<String>();
		MarsRover mr = new MarsRover(0, 0, planetObstacles);
		
		assertEquals("(9,0,W)", mr.executeCommand("lf"));
	}
	
	
	@Test
	public void testWrappingWithForwardMovementFromSouth() throws Exception {
		ArrayList<String> planetObstacles = new ArrayList<String>();
		MarsRover mr = new MarsRover(0, 0, planetObstacles);
		
		assertEquals("(0,9,S)", mr.executeCommand("llf"));
	}
	
	
	@Test
	public void testSingleFrontObstacle() throws Exception {
		ArrayList<String> planetObstacles = new ArrayList<String>();
		planetObstacles.add("(2,2)");
		MarsRover mr = new MarsRover(0, 0, planetObstacles);
		
		assertEquals("(1,2,E)(2,2)", mr.executeCommand("ffrfff"));
	}
	
	
	@Test
	public void testSingleBackObstacle() throws Exception {
		ArrayList<String> planetObstacles = new ArrayList<String>();
		planetObstacles.add("(2,2)");
		MarsRover mr = new MarsRover(0, 0, planetObstacles);
		
		assertEquals("(1,2,W)(2,2)", mr.executeCommand("rrbbrbbb"));
	}
	
	
	@Test
	public void testMultipleObstacle() throws Exception {
		ArrayList<String> planetObstacles = new ArrayList<String>();
		planetObstacles.add("(2,2)");
		planetObstacles.add("(2,1)");
		MarsRover mr = new MarsRover(0, 0, planetObstacles);
		
		assertEquals("(1,1,E)(2,2)(2,1)", mr.executeCommand("ffrfffrflf"));
	}
	
}
