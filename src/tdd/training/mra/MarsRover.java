package tdd.training.mra;

import java.util.ArrayList;
import java.util.List;

public class MarsRover {
	
	private static final String N = "N";
	private static final String S = "S";
	private static final String E = "E";
	private static final String W = "W";
	private static final int X_MAX = 10;
	private static final int Y_MAX = 10;
	
	private int planetX;
	private int planetY;
	private List<String> planetObstacles;
	private String actualDir;
	
	
	/**
	 * It initializes the rover at the coordinates (0,0), facing North, on a planet
	 * (represented as a grid with x and y coordinates) containing obstacles.
	 * 
	 * @param planetX         The x dimension of the planet.
	 * @param planetY         The y dimension of the planet.
	 * @param planetObstacles The obstacles on the planet. Each obstacle is a string
	 *                        (without white spaces) formatted as follows:
	 *                        "(oi_x,oi_y)". <code>null</code> if the planet does
	 *                        not contain obstacles.
	 * 
	 * @throws MarsRoverException
	 */
	public MarsRover(int planetX, int planetY, List<String> planetObstacles) throws MarsRoverException {
		this.planetX = planetX;
		this.planetY = planetY;
		this.planetObstacles = planetObstacles;
		this.actualDir = "N";
	}

	/**
	 * It returns whether, or not, the planet (where the rover moves) contains an
	 * obstacle in a cell.
	 * 
	 * @param x The x coordinate of the cell
	 * @param y The y coordinate of the cell
	 * @return <true> if the cell contains an obstacle, <false> otherwise.
	 * @throws MarsRoverException
	 */
	public boolean planetContainsObstacleAt(int x, int y) throws MarsRoverException {
		for(String coordinates : planetObstacles) {
			int xPos = getFirstCoordinate(coordinates);
			int yPos = getSecondCoordinate(coordinates);
			
			if(xPos==x && yPos==y) {
				return true;
			}
		}
		
		return false;
	}

	/**
	 * It lets the rover move on the planet according to a command string. The
	 * return string contains the new position of the rover, its direction, and the
	 * obstacles it has encountered while moving on the planet (if any).
	 * 
	 * @param commandString A string that can contain a single command -- i.e. "f"
	 *                      (forward), "b" (backward), "l" (left), or "r" (right) --
	 *                      or a combination of single commands.
	 * @return The return string that contains the position and direction of the
	 *         rover, and the obstacles the rover has encountered while moving on
	 *         the planet (if any). The return string (without white spaces) has the
	 *         following format: "(x,y,dir)(o1_x,o1_y)(o2_x,o2_y)...(on_x,on_y)". x
	 *         and y define the new position of the rover while dir represents its
	 *         direction (i.e., N, S, W, or E). Finally, oi_x and oi_y are the
	 *         coordinates of the i-th encountered obstacle.
	 * @throws MarsRoverException
	 */
	public String executeCommand(String commandString) throws MarsRoverException {
		ArrayList<String> obstacleCoordinates = new ArrayList<String>();
		String encounteredObstacles = "";
		
		if(commandString=="") {
			return "(0,0,N)";
		}
		
		for(int i=0; i<commandString.length(); i++) {
			String singleCommand = Character.toString(commandString.charAt(i));
			
			switch(singleCommand) {
			case "r": {
				actualDir = turnRight(actualDir);
				break;
			}
			case "l": {
				actualDir = turnLeft(actualDir);
				break;
			}
			case "f": {
				if(!hasObstacleOnFront()) {
					moveForward();
				}
				else {
					String singleObstacleCoordinate = getFrontObstaclesCoordinates();
					if(!obstacleCoordinates.contains(singleObstacleCoordinate)) {
						obstacleCoordinates.add(singleObstacleCoordinate);
						encounteredObstacles += singleObstacleCoordinate;
					}
				}
				break;
			}
			case "b": {
				if(!hasObstacleOnBack()) {
					moveBackward();
				}
				else {
					String singleObstacleCoordinate = getBackObstaclesCoordinates();
					if(!obstacleCoordinates.contains(singleObstacleCoordinate)) {
						obstacleCoordinates.add(singleObstacleCoordinate);
						encounteredObstacles += singleObstacleCoordinate;
					}
				}
				break;
			}
			
			}
		}
		
		return "(" + planetX + "," + planetY + "," + actualDir + ")" + encounteredObstacles;
	}
	
	
	/*
	 * Get first coordinate from a string.
	 */
	private static int getFirstCoordinate(String coordinate) {
		return Character.getNumericValue(coordinate.charAt(1));
	}
	
	/*
	 * Get first coordinate from a string.
	 */
	private static int getSecondCoordinate(String coordinate) {
		return Character.getNumericValue(coordinate.charAt(3));
	}
	
	/*
	 * Return the direction after turning right.
	 */
	private static String turnRight(String actualDir) {
		if(actualDir == N) {
			return E;
		}
		else if(actualDir == E) {
			return S;
		}
		else if(actualDir == S) {
			return W;
		}
		
		return N;
	}
	
	/*
	 * Return the direction after turning right.
	 */
	private static String turnLeft(String actualDir) {
		if(actualDir == N) {
			return W;
		}
		else if(actualDir == W) {
			return S;
		}
		else if(actualDir == S) {
			return E;
		}
		
		return N;
	}
	
	/*
	 * Moves rover forward.
	 */
	private void moveForward() {
		switch(actualDir) {
		
		case N: {
			planetY++;
			break;
		}
		case S: {
			planetY--;
			break;
		}
		case E: {
			planetX++;
			break;
		}
		case W: {
			planetX--;
			break;
		}
		
		}
		
		planetX = getCorrectXCoordinate(planetX);
		planetY = getCorrectYCoordinate(planetY);
	}
	
	
	/*
	 * Moves rover forward.
	 */
	private void moveBackward() {
		switch(actualDir) {
		
		case N: {
			planetY--;
			break;
		}
		case S: {
			planetY++;
			break;
		}
		case E: {
			planetX--;
			break;
		}
		case W: {
			planetX++;
			break;
		}
		
		}
		
		planetX = getCorrectXCoordinate(planetX);
		planetY = getCorrectYCoordinate(planetY);
	}
	
	/*
	 * Returns the correct X coordinate.
	 */
	private static int getCorrectXCoordinate(int x) {
		return (x+X_MAX) % X_MAX;
	}
	
	/*
	 * Returns the correct Y coordinate.
	 */
	private int getCorrectYCoordinate(int y) {
		return (y+Y_MAX) % Y_MAX;
	}
	
	/*
	 * Check if the rover has an obstacle on front.
	 */
	private boolean hasObstacleOnFront() throws MarsRoverException{
		if(actualDir==N && planetContainsObstacleAt(planetX, getCorrectYCoordinate(planetY+1))) {
			return true;
		}
		
		if(actualDir==E && planetContainsObstacleAt(getCorrectXCoordinate(planetX+1), planetY)) {
			return true;
		}
		
		if(actualDir==S && planetContainsObstacleAt(planetX, getCorrectYCoordinate(planetY-1))) {
			return true;
		}
		
		if(actualDir==W && planetContainsObstacleAt(getCorrectXCoordinate(planetX-1), planetY)) {
			return true;
		}
		
		return false;
	}
	
	/*
	 * Check if the rover has an obstacle on front.
	 */
	private boolean hasObstacleOnBack() throws MarsRoverException{
		if(actualDir==N && planetContainsObstacleAt(planetX, getCorrectYCoordinate(planetY-1))) {
			return true;
		}
		
		if(actualDir==E && planetContainsObstacleAt(getCorrectXCoordinate(planetX-1), planetY)) {
			return true;
		}
		
		if(actualDir==S && planetContainsObstacleAt(planetX, getCorrectYCoordinate(planetY+1))) {
			return true;
		}
		
		if(actualDir==W && planetContainsObstacleAt(getCorrectXCoordinate(planetX+1), planetY)) {
			return true;
		}
		
		return false;
	}
	
	/*
	 * Returns the coordinates of obstacle on front
	 */
	private String getFrontObstaclesCoordinates() {
		String coordinates = "";
		
		switch(actualDir) {
		
		case N: {
			coordinates = "(" + (planetX) + "," + getCorrectYCoordinate(planetY+1) + ")";
			break;
		}
		case E: {
			coordinates = "(" + getCorrectXCoordinate(planetX+1) + "," + (planetY) + ")";
			break;
		}
		case S: {
			coordinates = "(" + (planetX) + "," + getCorrectYCoordinate(planetY-1) + ")";
			break;
		}
		case W: {
			coordinates = "(" + getCorrectXCoordinate(planetX-1) + "," + (planetY) + ")";
			break;
		}
		
		}
		
		return coordinates;
	}
	
	
	/*
	 * Returns the coordinates of obstacle on front
	 */
	private String getBackObstaclesCoordinates() {
		String coordinates = "";
		
		switch(actualDir) {
		
		case N: {
			coordinates = "(" + (planetX) + "," + getCorrectYCoordinate(planetY-1) + ")";
			break;
		}
		case E: {
			coordinates = "(" + getCorrectXCoordinate(planetX-1) + "," + (planetY) + ")";
			break;
		}
		case S: {
			coordinates = "(" + (planetX) + "," + getCorrectYCoordinate(planetY+1) + ")";
			break;
		}
		case W: {
			coordinates = "(" + getCorrectXCoordinate(planetX+1) + "," + (planetY) + ")";
			break;
		}
		
		}
		
		return coordinates;
	}

}
